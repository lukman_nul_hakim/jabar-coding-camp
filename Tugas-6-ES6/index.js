//SOAL 1

const luasPersegip = (a, b) => a * b;{
}
const kelilingPersegip = (c,d) => 2 * (c + d); {
}

let hitungLuas = luasPersegip(7,5) //luas persegi panjang
console.log (hitungLuas)

let hitungKeliling = kelilingPersegip(18,7) // keliling persegi panjang
console.log (hitungKeliling)


//SOAL 2
const newFunction = (firstName, lastName) => {
    return {
        fullName: function(){
            console.log(`${firstName} ${lastName}`)
      }
    }
}
   
  //Driver Code 
  newFunction("William", "Imoh").fullName()


//SOAL 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }


const {firstName,lastName,address,hobby} = newObject // with destructuring

// Driver code
console.log(firstName, lastName, address, hobby)



//SOAL 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]


let combined = [...west, ...east] //ES6 Way - combining array
console.log(combined)


//SOAL 5
const planet = "earth" 
const view = "glass"
let after = ` Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} ` // template literal

console.log (after)