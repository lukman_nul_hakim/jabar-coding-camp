// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(pertama.substring(0,4),pertama.substring(11,18),kedua.substring(0,7),kedua.substring(8,19).toUpperCase());

// soal 2
var kataPertama = Number ("10");
var kataKedua = Number ("2");
var kataKetiga = Number ("4");
var kataKeempat = Number ("6");

var result = (kataPertama % kataKedua) + (kataKetiga * kataKeempat);
console.log (result);

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0,3); 
var kataKedua   = kalimat.substring(4,14);// I did it! 
var kataKetiga  = kalimat.substring(15,18);// I did it! 
var kataKeempat = kalimat.substring(19,24); // i did it! 
var kataKelima  = kalimat.substring(25,33);  // i did it ! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

